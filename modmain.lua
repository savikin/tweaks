local TUNING = GLOBAL.TUNING
local ACTIONS = GLOBAL.ACTIONS
local require = GLOBAL.require

-- walls tweak is broken without at least one of DLC
-- thenceforth I will ignore walls altogether if I don't have any DLC
local DLC_ENABLED = false
if (GLOBAL.IsDLCEnabled(GLOBAL.REIGN_OF_GIANTS)) then
	DLC_ENABLED = true
end
if (GLOBAL.IsDLCEnabled(GLOBAL.CAPY_DLC)) then
	DLC_ENABLED = true
end
if (GLOBAL.IsDLCEnabled(GLOBAL.PORKLAND_DLC)) then
	DLC_ENABLED = true
end

local BOOMERANG_SPEED = GetModConfigData("BOOMERANG_SPEED")
local BOOST_GOLD = GetModConfigData("BOOST_GOLD")
local BOOST_GOLD_DAMAGE = GetModConfigData("BOOST_GOLD_DAMAGE")
local BOOST_OBSIDIAN = GetModConfigData("BOOST_OBSIDIAN")
local BOOST_OBSIDIAN_DAMAGE = GetModConfigData("BOOST_OBSIDIAN_DAMAGE")
local BOOST_MULTITOOL = GetModConfigData("BOOST_MULTITOOL")
local BOOST_MULTITOOL_DAMAGE = GetModConfigData("BOOST_MULTITOOL_DAMAGE")
local ICEBOX_TUNING = GetModConfigData("ICEBOX_TUNING")
local PERISHING_TUNING = GetModConfigData("PERISHING_TUNING")
local TORCH_RADIUS = GetModConfigData("TORCH_RADIUS")
local OVERRIDE = GetModConfigData("GLOBAL_DURABILITY_OVERRIDE")

-- Stack size modifying
local function STACK_MODIFYING_FUNCTION(value_modifier)
	TUNING.STACK_SIZE_LARGEITEM = TUNING.STACK_SIZE_LARGEITEM * value_modifier
	TUNING.STACK_SIZE_MEDITEM   = TUNING.STACK_SIZE_MEDITEM * value_modifier
	TUNING.STACK_SIZE_SMALLITEM = TUNING.STACK_SIZE_SMALLITEM * value_modifier
end
STACK_MODIFYING_FUNCTION(GetModConfigData("STACK_SIZE_MODIFIER"))


-- Remove % on itemslot for infinte items
local ItemTile = require "widgets/itemtile"
local originalSetPercent = ItemTile.SetPercent
function ItemTile:SetPercent(percent)
    if not self.item.components.inventoryitem or not self.item:HasTag("Infinite") then
        originalSetPercent(self, percent)
    end
end

local function TuningDurability(inst, factor)
	local function tune(component, ...)
		if component then
			local fields, original = { ... }, component.original
			if not original then
				original = {}
				for _, field in ipairs(fields) do
					original[field] = component[field]
				end
				component.original = original
			end

			for _, field in ipairs(fields) do
				if original[field] and component[field] then
                    --stumbled upon this looking while looking for 
                    --the reason that lowering of durability did not work
                    -- old code:
					--component[field] = math.max(component[field], original[field] * factor)
                    -- new code
					component[field] = original[field] * factor
				end
			end
		end
	end

	tune(inst.components.finiteuses, 'total', 'current')
	tune(inst.components.perishable, 'perishtime', 'perishremainingtime')
	tune(inst.components.fueled, 'maxfuel', 'currentfuel', 'bonusmult')
	tune(inst.components.armor, 'maxcondition', 'condition')
	tune(inst.components.boathealth, 'maxhealth', 'currenthealth')
end

local function RemoveDurability(inst)
	inst:AddTag("Infinite")

	if inst.components.finiteuses then
		inst.components.finiteuses.Use = function()
      inst.components.finiteuses.current = inst.components.finiteuses.total
    end
    inst.components.finiteuses.current = inst.components.finiteuses.total
	end

	if inst.components.perishable then
		inst.components.perishable.StartPerishing = function() end
		inst.components.perishable:StopPerishing()
	end

	if inst.components.fueled then
		inst.components.fueled.StartConsuming = function()
      inst.components.fueled.currentfuel = inst.components.fueled.maxfuel
    end
		inst.components.fueled.DoDelta = function()
      inst.components.fueled.currentfuel = inst.components.fueled.maxfuel
    end

		inst.components.fueled.currentfuel = inst.components.fueled.maxfuel
	end

	if inst.components.armor then
		inst.components.armor.SetCondition = function() end
	end

	if inst.components.boathealth then
		inst.components.boathealth.DoUpdate = function()
      inst.components.boathealth.LeakingHealth = 0
      TUNING.WAVE_HIT_DAMAGE = 0
      TUNING.ROGUEWAVE_HIT_DAMAGE = 0
    end

		inst.components.boathealth.LeakingHealth = 0
		TUNING.WAVE_HIT_DAMAGE = 0
		TUNING.ROGUEWAVE_HIT_DAMAGE = 0
	end

	--walls tweak breaks world generation without DLC
	if inst.components.health and inst:HasTag("wall") and DLC_ENABLED then
		inst.components.health:SetAbsorptionAmount(1)
	end
end

local function Tweak(prefabs, option)
	if option ~= "Default" then
		local function TweakNumberOption(inst)
			TuningDurability(inst, option)
		end
		-- yeap, lua ternaries are weird
		local tweak_function = option == "Infinite" and RemoveDurability or TweakNumberOption
		local processed = {}
		for _, prefab in ipairs(prefabs) do
			if not processed[prefab] then
				AddPrefabPostInit(prefab, tweak_function)
				processed[prefab] = true
			end
		end
	end
end

local DURABILITIES = {
	BOATS_DURABILITY = {
		"armouredboat",
		"cargoboat",
		"corkboat",
		"encrustedboat",
		"lograft",
		"raft",
		"rowboat",
		"surfboard",
		"woodlegsboat",
	},
	WALLS_DURABILITY = {
		"wall_enforcedlimestone",
		"wall_hay",
		"wall_limestone",
		"wall_ruins",
		"wall_stone",
		"wall_wood",
	},
	WEAPON_DURABILITY = {
		"batbat",
		"boomerang",
		"cork_bat",
		"halberd",
		"hambat",
		"harpoon",
		"needlespear",
		"nightstick",
		"peg_leg",
		"ruins_bat",
		"spear",
		"spear_obsidian",
		"spear_poison",
		"spear_wathgrithr",
		"tentaclespike",
		"trident",
	},
	PROJECTILE_DURABILITY = {
		"boomerang",
		"harpoon",
		"spear_launcher",
		"spear_launcher_obsidian",
		"spear_launcher_poison",
		"spear_launcher_spear",
		"spear_launcher_wathgrithr",
	},
	SWORD_DURABILITY = {
		"nightsword",
		"cutlass",
	},
	ARMOR_DURABILITY = {
		"antsuit",
		"armor_bramble",
		"armor_metalplate",
		"armor_sanity",
		"armor_weevole",
		"armor_windbreaker",
		"armorcactus",
		"armordragonfly",
		"armorgrass",
		"armorlimestone",
		"armormarble",
		"armorobsidian",
		"armorruins",
		"armorseashell",
		"armorsnurtleshell",
		"armorvortexcloak",
		"armorwood",
		"beehat",
		"blubbersuit",
		"footballhat",
		"gogglesarmorhat",
		"oxhat",
		"ruinshat",
		"slurtlehat",
		"tarsuit",
		"wathgrithrhat",
	},
	STAFF_DURABILITY = {
		"firestaff",
		"greenstaff",
		"icestaff",
		"orangestaff",
		"staff_tornado",
		"telestaff",
		"yellowstaff",
		"telebrella",

		-- DLC: Shipwrecked
		"volcanostaff",
		"sail_stick",
	},
	DECONSTRUCTION_STAFF_DURABILITY = {
		"greenstaff",
	},
	AMULET_DURABILITY = {
		"amulet",
		"blueamulet",
		"greenamulet",
		"yellowamulet",
		"purpleamulet",
		"orangeamulet",
	},
	TOOL_DURABILITY = {
		"antler",
		"bell",
		"brush",
		"bugnet",
		"bugrepellent",
		"featherfan",
		"fertilizer",
		"firesuppressor",
		"fishingrod",
		"horn",
		"monkeyball",
		"ox_flute",
		"panflute",
		"saddle_basic",
		"saddle_race",
		"saddle_war",
		"saddlehorn",
		"tropicalfan",
		"walkingstick",
		"wind_conch",
	},
	SEWINGKIT_DURABILITY = {
		"sewing_kit",
	},
	COMPASS_DURABILITY = {
		"compass",
	},
	FLINT_DURABILITY = {
		"axe",
		"pickaxe",
		"shovel",
		"hammer",
		"pitchfork",
		"multitool_axe_pickaxe",
		"machete",
		"shears",
	},
	GOLD_DURABILITY = {
		"goldenaxe",
		"goldenpickaxe",
		"goldenshovel",
		"goldenmachete",
	},
	TRAP_DURABILITY = {
		"birdtrap",
		"seatrap",
		"trap",
		"trap_bramble",
		"trap_teeth",
	},
	CLOTHING_DURABILITY = {
		"aerodynamichat",
		"antmaskhat",
		"armor_snakeskin",
		"armorslurper",
		"bandithat",
		"beargervest",
		"beefalohat",
		"brainjellyhat",
		"captainhat",
		"catcoonhat",
		"double_umbrellahat",
		"earmuffshat",
		"eyebrellahat",
		"featherhat",
		"flowerhat",
		"gashat",
		"gasmaskhat",
		"gogglesheathat",
		"gogglesnormalhat",
		"gogglesshoothat",
		"grass_umbrella",
		"hawaiianshirt",
		"icehat",
		"metalplatehat",
		"onemanband",
		"palmleaf_umbrella",
		"piratehat",
		"pithhat",
		"raincoat",
		"rainhat",
		"reflectivevest",
		"shark_teethhat",
		"snakeskinhat",
		"spiderhat",
		"strawhat",
		"sweatervest",
		"thunderhat",
		"tophat",
		"trunkvest_summer",
		"trunkvest_winter",
		"umbrella",
		"walrushat",
		"watermelonhat",
		"winterhat",
		"wornpiratehat",
	},
	CAMPFIRE_DURABILITY = {
		"campfire",
		"coldfire",
	},
	FIREPIT_DURABILITY = {
		"chiminea",
		"coldfirepit",
		"firepit",
		"obsidianfirepit",
		"sea_chiminea",
	},
	LIGHT_DURABILITY = {
		"bathat",
		"boat_lantern",
		"boat_torch",
		"bottlelantern",
		"candlehat",
		"lantern",
		"lighter",
		"minerhat",
		"nightlight",
		"pumpkin_lantern",
		"tarlamp",
		"torch",
		"molehat",
	},
	CAMPING_DURABILITY = {
		"bedroll_furry",
		"siestahut",
		"tent",
	},
	BOOK_DURABILITY = {
		"book_birds",
		"book_brimstone",
		"book_gardening",
		"book_sleep",
		"book_tentacles",
		"book_meteor",
	},
	SAILS_DURABILITY = {
		"clothsail",
		"feathersail",
		"ironwind",
		"sail",
		"snakeskinsail",
	},
	BOATITEMS_DURABILITY = {
		"boatcannon",
		"telescope",
		"supertelescope",
	},
	BOATREPAIRKIT_DURABILITY = {
		"boatrepairkit"
	},
	OBSIDIAN_DURABILITY = {
		"obsidianmachete",
		"obsidianaxe",
		"spear_obsidian"
	},
	TREASUREHUNTING_DURABILITY = {
		"ballpein_hammer",
		"disarming_kit",
		"goldpan",
		"magnifying_glass",
	},
}

-- Section: durability activation
-- Items durabilities
if OVERRIDE == false then
  for option, prefabs in pairs(DURABILITIES) do
    Tweak(prefabs, GetModConfigData(option))
  end
else
  for option, prefabs in pairs(DURABILITIES) do
    Tweak(prefabs, OVERRIDE)
  end
end

-- Section:
-- Efficacy settings and misc tunings
if (BOOST_GOLD ~= 1 or BOOST_GOLD_DAMAGE ~= 1) then
  AddPrefabPostInit("goldenaxe", function(inst)
	inst.components.weapon:SetDamage(TUNING.AXE_DAMAGE * BOOST_GOLD_DAMAGE)
	inst.components.tool:SetAction(ACTIONS.CHOP, BOOST_GOLD)
  end)
  AddPrefabPostInit("goldenpickaxe", function(inst)
    inst.components.weapon:SetDamage(TUNING.PICK_DAMAGE * BOOST_GOLD_DAMAGE)
    inst.components.tool:SetAction(ACTIONS.MINE, BOOST_GOLD)
  end)
  AddPrefabPostInit("goldenmachete", function(inst)
    inst.components.weapon:SetDamage(TUNING.MACHETE_DAMAGE * BOOST_GOLD_DAMAGE)
    inst.components.tool:SetAction(ACTIONS.HACK, BOOST_GOLD)
  end)
  AddPrefabPostInit("goldenshovel", function(inst)
    inst.components.weapon:SetDamage(TUNING.SHOVEL_DAMAGE * BOOST_GOLD_DAMAGE)
  end)
end

if (BOOST_OBSIDIAN ~= 1 or BOOST_OBSIDIAN_DAMAGE ~= 1) then
	AddPrefabPostInit("obsidianaxe", function(inst)
		inst.components.weapon:SetDamage(TUNING.AXE_DAMAGE * BOOST_OBSIDIAN_DAMAGE)
		inst.components.tool:SetAction(ACTIONS.CHOP, BOOST_OBSIDIAN)
	end)
	AddPrefabPostInit("obsidianmachete", function(inst)
		inst.components.weapon:SetDamage(TUNING.MACHETE_DAMAGE * BOOST_OBSIDIAN_DAMAGE)
		inst.components.tool:SetAction(ACTIONS.HACK, BOOST_OBSIDIAN)
	end)
	AddPrefabPostInit("spear_obsidian", function(inst)
		inst.components.weapon:SetDamage(TUNING.SPEAR_DAMAGE * BOOST_OBSIDIAN_DAMAGE)
	end)
end

if (BOOST_MULTITOOL ~= 1 or BOOST_MULTITOOL_DAMAGE ~= 1) then
	AddPrefabPostInit("multitool_axe_pickaxe", function(inst)
		inst.components.weapon:SetDamage(TUNING.AXE_DAMAGE * BOOST_GOLD_DAMAGE)
		inst.components.tool:SetAction(ACTIONS.CHOP, BOOST_MULTITOOL)
		inst.components.tool:SetAction(ACTIONS.MINE, BOOST_MULTITOOL)
	end)
end

if BOOMERANG_SPEED ~= 10 then
	AddPrefabPostInit("boomerang", function(inst)
		inst.components.projectile:SetSpeed(BOOMERANG_SPEED)
	end)
end

if TORCH_RADIUS ~= "Default" then
	AddPrefabPostInit("torchfire", function(inst)
		if inst and inst.Light then
			inst.Light:SetRadius(TORCH_RADIUS)
		end
	end)
end

if ICEBOX_TUNING ~= "Default" then
	TUNING.PERISH_FRIDGE_MULT = ICEBOX_TUNING
end

if PERISHING_TUNING ~= "Default" then
	TUNING.PERISH_PRESERVED = TUNING.PERISH_PRESERVED * PERISHING_TUNING
	TUNING.PERISH_SUPERSLOW = TUNING.PERISH_SUPERSLOW * PERISHING_TUNING
	TUNING.PERISH_SLOW = TUNING.PERISH_SLOW * PERISHING_TUNING
	TUNING.PERISH_MED = TUNING.PERISH_MED * PERISHING_TUNING
	TUNING.PERISH_FAST = TUNING.PERISH_FAST * PERISHING_TUNING
	TUNING.PERISH_SUPERFAST = TUNING.PERISH_SUPERFAST * PERISHING_TUNING
	TUNING.PERISH_ONE_DAY = TUNING.PERISH_ONE_DAY * PERISHING_TUNING
	-- TODO: I don't remember why this is behind the DLC check, need to figure that out
	if DLC_ENABLED then
		TUNING.PERISH_FASTISH = TUNING.PERISH_FASTISH * PERISHING_TUNING
	end
end


