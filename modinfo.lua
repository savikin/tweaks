name = "Durability Tweaks"
version = "4.0.36"
description = "Tweaks durabilities, v" .. version
author = "AngeloData"

forumthread = ""

api_version = 6

icon_atlas = "DurabilityTweaks.xml"
icon = "DurabilityTweaks.tex"

dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true
hamlet_compatible = true
dst_compatible = false

local options_switch = {
	{description = "Yes", data = true},
	{description = "No", data = false},
}

local options_durability_optional = {
	{description = "No", data = false},
	{description = "10%", data = 0.1},
	{description = "25%", data = 0.25},
	{description = "50%", data = 0.5},
	{description = "75%", data = 0.75},
	{description = "100%", data = 1},
	{description = "150%", data = 1.5},
	{description = "200%", data = 2},
	{description = "300%", data = 3},
	{description = "400%", data = 4},
	{description = "500%", data = 5},
	{description = "750%", data = 7.5},
	{description = "1000%", data = 10},
	{description = "Infinite", data = "Infinite"},
}

local options_durability = {
	{description = "10%", data = 0.1},
	{description = "25%", data = 0.25},
	{description = "50%", data = 0.5},
	{description = "75%", data = 0.75},
	{description = "Default", data = "Default"},
	{description = "150%", data = 1.5},
	{description = "200%", data = 2},
	{description = "300%", data = 3},
	{description = "400%", data = 4},
	{description = "500%", data = 5},
	{description = "750%", data = 7.5},
	{description = "1000%", data = 10},
	{description = "Infinite", data = "Infinite"},
}

local options_damage = {
	{description = "Default", data = 1},
	{description = "150%", data = 1.5},
	{description = "200%", data = 2},
	{description = "250%", data = 2.5},
	{description = "300%", data = 3},
	{description = "400%", data = 4},
	{description = "500%", data = 5},
}

local options_efficacy = {
	{description = "Default", data = 1},
	{description = "x2", data = 2},
	{description = "x3", data = 3},
	{description = "x4", data = 4},
	{description = "x5", data = 5},
	{description = "x6", data = 6},
	{description = "x7", data = 7},
	{description = "x8", data = 8},
	{description = "x9", data = 9},
	{description = "x10", data = 10},
	{description = "x10000", data = 10000},
}

configuration_options =
{
	{
		name = "INTERSECTION_1",
		label = "Tuning",
		options = {{description = "", data = ""}},
		default = "",
	},
	{
		name = "ICEBOX_TUNING",
		label = "Food freezing efficacy",
		options = {
			{description = "Default", data = "Default"},
			{description = "Stasis", data = 0},
			{description = "Rot", data = 1e100},
			{description = "Refreshing", data = -10},
		},
		default = "Default",
	},
	{
		name = "PERISHING_TUNING",
		label = "Perishing time",
		options = options_efficacy,
		default = "Default",
	},
	{
		-- DEPRECATED
		name = "HEATROCK_DURABILITY",
		label = "Heatrock",
		options = {
			{description = "Deprecated", data = false},
		},
		default = false,
	},
	{
		name = "BOOMERANG_SPEED",
		label = "Tweak boomerang speed",
		options = {
			{description = "50%", data = 5},
			{description = "Default", data = 10},
			{description = "150%", data = 15},
			{description = "200%", data = 20},
			{description = "250%", data = 25},
			{description = "300%", data = 30},
			{description = "350%", data = 35},
			{description = "400%", data = 40},
			{description = "500%", data = 50},
		},
		default = 10,
	},

	{
		name = "BOOST_GOLD_DAMAGE",
		label = "Gold tools damage",
		options = options_damage,
		default = 1,
	},

	{
		name = "BOOST_OBSIDIAN_DAMAGE",
		label = "Obsidian tools damage",
		options = options_damage,
		default = 1,
	},

	{
		name = "BOOST_MULTITOOL_DAMAGE",
		label = "Pick/Axe damage",
		options = options_damage,
		default = 1,
	},

	{
		name = "BOOST_GOLD",
		label = "Gold tools efficacy",
		options = options_efficacy,
		default = 1,
	},


	{
		name = "BOOST_OBSIDIAN",
		label = "Obsidian tools efficacy",
		options = options_efficacy,
		default = 1,
	},


	{
		name = "BOOST_MULTITOOL",
		label = "Pick/Axe efficacy",
		options = options_efficacy,
		default = 1,
	},

	{
		name = "TORCH_RADIUS",
		label = "Change torch light radius",
		options = {
			{description = "50%", data = 1},
			{description = "Default", data = "Default"},
			{description = "150%", data = 3},
			{description = "200%", data = 4},
			{description = "250%", data = 5},
		},

		default = "Default",
	},
	{
		name = "STACK_SIZE_MODIFIER",
		label = "Stack size modifier",
		options = {
			{description = "25%", data = 0.25},
			{description = "50%", data = 0.5},
			{description = "75%", data = 0.75},
			{description = "Default", data = 1},
			{description = "200%", data = 2},
			{description = "300%", data = 3},
			{description = "500%", data = 5},
			{description = "1000%", data = 10},
			{description = "10000%", data = 100},
		},
		default = 1
	},
	{
		name = "GLOBAL_DURABILITY_OVERRIDE",
		label = "Override all durabilities",
		options = options_durability_optional,
		default = false
	},
	{
		name = "INTERSECTION_2",
		label = "Individual durabilities",
		options = {{description = "", data = ""}},
		default = "",
	},

	{
		name = "PROJECTILE_DURABILITY",
		label = "Projectiles",
		options = options_durability,
		default = "Default",
	},

	{
		name = "WEAPON_DURABILITY",
		label = "Weapons",
		options = options_durability,
		default = "Default",
	},

	{
		name = "SWORD_DURABILITY",
		label = "Swords only",
		options = options_durability,
		default = "Default",
	},

	{
		name = "ARMOR_DURABILITY",
		label = "Armor",
		options = options_durability,
		default = "Default",
	},

	{
		name = "STAFF_DURABILITY",
		label = "Staffs",
		options = options_durability,
		default = "Default",
	},
	
	{
		name = "DECONSTRUCTION_STAFF_DURABILITY",
		label = "Deconstruction staff",
		options = options_durability,
		default = "Default",
	},

	{
		name = "AMULET_DURABILITY",
		label = "Amulets",
		options = options_durability,
		default = "Default",
	},

	{
		name = "TOOL_DURABILITY",
		label = "Survival tools",
		options = options_durability,
		default = "Default",
	},

	{
		name = "SEWINGKIT_DURABILITY",
		label = "Sewing kit",
		options = options_durability,
		default = "Default",
	},

	{
		name = "COMPASS_DURABILITY",
		label = "Compass",
		options = options_durability,
		default = "Default",
	},

	{
		name = "FLINT_DURABILITY",
		label = "Flint tools",
		options = options_durability,
		default = "Default",
	},

	{
		name = "GOLD_DURABILITY",
		label = "Gold items",
		options = options_durability,
		default = "Default",
	},

	{
		name = "OBSIDIAN_DURABILITY",
		label = "Obsidian items",
		options = options_durability,
		default = "Default",
	},

	{
		name = "TRAP_DURABILITY",
		label = "Traps",
		options = options_durability,
		default = "Default",
	},
	{
		name = "CLOTHING_DURABILITY",
		label = "Clothing",
		options = options_durability,
		default = "Default",
	},

	{
		name = "CAMPFIRE_DURABILITY",
		label = "Campfires",
		options = options_durability,
		default = "Default",
	},

	{
		name = "FIREPIT_DURABILITY",
		label = "Firepits",
		options = options_durability,
		default = "Default",
	},

	{
		name = "LIGHT_DURABILITY",
		label = "Light sources",
		options = options_durability,
		default = "Default",
	},
	

	{
		name = "CAMPING_DURABILITY",
		label = "Sleep items",
		options = options_durability,
		default = "Default",
	},

	{
		name = "BOOK_DURABILITY",
		label = "Books",
		options = options_durability,
		default = "Default",
	},

		
	{
		name = "SAILS_DURABILITY",
		label = "Sails & IronWind",
		options = options_durability,
		default = "Default",
	},

	{
		name = "BOATITEMS_DURABILITY",
		label = "Telescope & cannon",
		options = options_durability,
		default = "Default",
	},

	{
		name = "BOATREPAIRKIT_DURABILITY",
		label = "Boat repairkit",
		options = options_durability,
		default = "Default",
	},

	{
		name = "TREASUREHUNTING_DURABILITY",
		label = "Treasure hunting tools",
		options = options_durability,
		default = "Default",
	},

	{
		name = "BOATS_DURABILITY",
		label = "Boats",
		options = options_durability,
		default = "Default",
	},

	{
		name = "WALLS_DURABILITY",
		label = "Walls",
		options = {
			{description = "Default", data = "Default"},
			{description = "Infinite", data = "Infinite"},
		},
		default = "Default",
	},

}
